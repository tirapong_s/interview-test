USE [StandByDB]
GO

/****** Object:  StoredProcedure [dbo].[SP_TESTTRUE]    Script Date: 31/7/2563 18:04:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_TESTTRUE]
 @Khet int  = null ,
 @Khwang int   = null,
 @Province int  = null,
 @email nvarchar(50)   = null,
 @first_name nvarchar(50)  = null,
 @last_name nvarchar(50)  = null,
 @username nvarchar(50)   = null,
 @zipcode int  = null
AS
BEGIN

	SET NOCOUNT ON;
	SELECT * FROM [StandByDB].[dbo].[tb_user]
	INSERT       INTO tb_user(user_id, username, first_name, last_name, create_by, create_date, last_update_by, last_update_date)
	VALUES        ( (SELECT max([id]) FROM [StandByDB].[dbo].[tb_user]) + 1, @username,@first_name,@last_name,'System',GETDATE(),'System',GETDATE())

	INSERT   INTO  tb_address(UserID, ProvinceID, KhetID, KhwangID, Zipcode)
VALUES        ((SELECT max([id]) FROM [StandByDB].[dbo].[tb_user]) ,  @Province , @Khet, @Khwang, @zipcode)
END
GO

