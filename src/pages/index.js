import React, { Component } from "react";
import { Dropdown, Menu, Input, Select, Table, Modal, Button } from "antd";
import { Api } from "../service";
import axios from "axios";

class MaxiOperation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      visible: false,
      dataSource: [],
      modaultitle: "",
      columns: [
        {
          title: 'No',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: 'User Id',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: 'Username',
          dataIndex: 'username',
          key: 'username',
        },
        {
          title: 'First Name',
          dataIndex: 'first_name',
          key: 'first_name',
        },
        {
          title: 'Last Name',
          dataIndex: 'last_name',
          key: 'last_name',
        },
        {
          title: 'Create By',
          dataIndex: 'create_by',
          key: 'create_by',
        },
        {
          title: 'Create Date',
          dataIndex: 'create_date',
          key: 'create_date',
        },
        {
          title: 'Last Update By',
          dataIndex: 'last_update_by',
          key: 'last_update_by',
        },
        {
          title: 'Last Update Date',
          dataIndex: 'last_update_date',
          key: 'last_update_date',
        },
        {
          title: 'edit',
          dataIndex: 'edit',
          key: 'edit',
          render: (item) => (
            <div>

              <a
                onClick={() =>
                  this.setState({ visible: true, modaultitle: "Edit Username" })
                }
                href="#"
              >
                Edit
              </a>

            </div>
          ),
        },
        {
          title: 'delete',
          dataIndex: '',
          key: '',
          render: (item) => (
            <div>

              <a
                onClick={() =>
                  // this.setState({ visible: true })
                  // console.log(item)
                  axios.post(`http://localhost:3001/deleteuser`, { id: item.id })
                    .then(res => {
                      const persons = res.data;
                      if (persons.recordsets) {
                        this.setState({ visible: false });
                        window.location.reload();
                      }
                      else {
                        alert("error");
                      }
                    })
                }
                href="#"
              >
                Delete
              </a>

            </div>
          ),
        },
      ],
      form: [],
      select_Province: [],
      select_Khet: [],
      select_Khwang: [],
      select_zipcode: [],
      username: "",
      first_name: "",
      last_name: "",
      email: "",
      Province: "",
      Khet: "",
      Khwang: "",

    }
    this.handleInputChange = this.handleInputChange.bind(this);
  }


  async componentDidMount() {
    axios.get(`http://localhost:3001/getUser`)
      .then(res => {
        const persons = res.data;
        console.log(persons.recordsets);
        this.setState({ dataSource: persons.recordset });
      })

  }

  showModal = () => {
    this.setState({
      visible: true,
      modaultitle: "Add Username"
    });

    axios.get(`http://localhost:3001/getProvince`)
      .then(res => {
        const persons = res.data;
        console.log(persons.recordsets);
        this.setState({ select_Province: persons.recordset });
      })

  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleInputChange(event) {
    var name = event.target.name;
    var value = event.target.value;
    var form = this.state.form;

    console.log(name);
    if (name === "username") {
      this.setState({ username: value });
    }
    if (name === "first_name") {
      this.setState({ first_name: value });
    }
    if (name === "last_name") {
      this.setState({ last_name: value });
    }
    if (name === "email") {
      this.setState({ email: value });
    }
    if (name === "zipcode") {
      this.setState({ zipcode: value });
    }
  }


  onChangeasset = (val, obj) => {
    if (obj.name === "Province") {
      this.setState({ Khwang: [], Khet: [] });
      axios.post(`http://localhost:3001/getKhet`, { id: val })
        .then(res => {
          const persons = res.data;
          this.setState({
            select_Khet: persons.recordset,
            Province: val
          });
        })
    }
    if (obj.name === "Khet") {
      axios.post(`http://localhost:3001/getKhwan`, { id: val })
        .then(res => {
          const persons = res.data;
          this.setState({ select_Khwang: persons.recordset, Khet: val });
        })
    }
    if (obj.name === "Khwang") {

      this.setState({ Khwang: val, select_zipcode: obj.obj.Zipcode });
    }

  };

  handleSubmit = async () => {

    // await axios.post(`http://localhost:3001/testpost`, this.state.form)
    var setdata = {
      username: this.state.username,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      Province: this.state.Province,
      Khet: this.state.Khet,
      Khwang: this.state.Khwang,
      zipcode: this.state.Khwang
    }
    axios.post(`http://localhost:3001/testpost`, setdata)
      .then(res => {
        const persons = res.data;
        // this.setState({ Khet: persons.recordset });
        if (persons.recordsets) {
          this.setState({ visible: false });
          window.location.reload();
        }
        else {
          alert("error");
        }
      })
      .then(res => {
        console.log(res);
      })
  };

  render() {

    return <div>
      {/* container */}
      <div className=" mt-5">
        <Button type="primary" onClick={this.showModal}>
          Add
        </Button>
        <Table dataSource={this.state.dataSource} columns={this.state.columns} />
      </div>

      <Modal
        title={this.state.modaultitle}
        visible={this.state.visible}
        footer={null}
        closable={false}
      >

        <div className="container">
          <div className="row">
            <div className="col-3">
              <label> Username</label>
            </div>
            <div className="col-6">
              <input type="text" name="username"
                onChange={this.handleInputChange} />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <label> First Name	</label>
            </div>
            <div className="col-6">
              <input type="text" name="first_name"
                onChange={this.handleInputChange} />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <label> Last Name</label>
            </div>
            <div className="col-6">
              <input type="text" name="last_name"
                onChange={this.handleInputChange} />
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <label> Email</label>
            </div>
            <div className="col-6">
              <input type="text" name="email"
                onChange={this.handleInputChange} />
            </div>
          </div>

          <div className="row">
            <div className="col-3">
              <label> Address</label>
            </div>
            <div className="col-6">

            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <label> Province</label>
            </div>
            <div className="col-6">
              <Select
                // showSearch
                style={{ border: "0px" }}
                placeholder="Select a Province"
                className="inputfile"
                style={{ width: "100%" }}
                onChange={this.onChangeasset}
              >
                {this.state.select_Province.map((item, index) => (
                  <Select.Option
                    value={item.ProvinceID}
                    name="Province"
                    key={index}
                    obj={item}
                  >
                    {item.ProvinceName}
                  </Select.Option>
                ))}
              </Select>
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <label> Khet</label>
            </div>
            <div className="col-6">
              <Select
                // showSearch
                style={{ border: "0px" }}
                placeholder="Select a Khet"
                className="inputfile"
                style={{ width: "100%" }}
                onChange={this.onChangeasset}
              >
                {this.state.select_Khet.map((item, index) => (
                  <Select.Option
                    value={item.KhetID}
                    key={index}
                    obj={item}
                    name="Khet"
                  >
                    {item.KhetName}
                  </Select.Option>
                ))}
              </Select>
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <label> Khwang</label>
            </div>
            <div className="col-6">
              <Select
                // showSearch
                style={{ border: "0px" }}
                placeholder="Select a Khwang"
                className="inputfile"
                style={{ width: "100%" }}
                onChange={this.onChangeasset}
              >
                {this.state.select_Khwang.map((item, index) => (
                  <Select.Option
                    value={item.KhwangID}
                    key={index}
                    obj={item}
                    name="Khwang"
                  >
                    {item.KhwangName}
                  </Select.Option>
                ))}
              </Select>
            </div>
          </div>
          <div className="row">
            <div className="col-3">
              <label> Zipcode</label>
            </div>
            <div className="col-6">
              <input type="text" name="zipcode" value={this.state.select_zipcode}
                onChange={this.handleInputChange} />
            </div>
          </div>
          <div style={{ textAlign: "center", marginTop: "5%" }}>


            <Button onClick={() => this.setState({ visible: false })}>Cancel</Button>
            <Button type="primary ml-3" onClick={this.handleSubmit}>Add</Button>

          </div>
        </div>
      </Modal >
    </div >
  }
}




export default MaxiOperation;
