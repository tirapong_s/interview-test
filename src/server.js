
var express = require('express');
var body = require('body-parser');
var mssql = require("mssql");


var app = express();
app.use(body());
app.use(body.json());

var config = {

};

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST")
    next()
})


app.get('*/getUser', function (req, res) {
    var qury = "SELECT * FROM [StandByDB].[dbo].[tb_user]";
    query(qury, function (record) {
        res.json(record);
    });
});

app.get('*/getProvince', function (req, res) {
    var qury = "SELECT  [ProvinceID] ,[ProvinceName] FROM [StandByDB].[dbo].[tb_Province]";
    query(qury, function (record) {
        res.json(record);
    });
});

app.post('*/getKhet', function (req, res) {
    var qury = "SELECT [ProvinceID],[KhetName] ,[KhetID] FROM [StandByDB].[dbo].[tb_Khet] where ProvinceID = " + req.body.id;
    query(qury, function (record) {
        res.json(record);
    });
});


app.post('*/getKhwan', function (req, res) {
    var qury = "SELECT [KhwangID],[KhwangName],[Zipcode] FROM [StandByDB].[dbo].[tb_Khwang] where KhetID = " + req.body.id;
    query(qury, function (record) {
        res.json(record);
    });
});


app.post('*/deleteuser', function (req, res) {
    var qury = "  delete  [StandByDB].[dbo].[tb_user] where id = " + req.body.id;
    query(qury, function (record) {
        res.json(record);
        });
});
app.post('*/testpost', function (req, res) {
    var qury = "exec [dbo].[SP_TESTTRUE] " +
        req.body.Khet
        + "," +
        req.body.Khwang
        + "," +
        req.body.Province
        + ",'" +
        req.body.email
        + "','" +
        req.body.first_name
        + "','" +
        req.body.last_name
        + "','" +
        req.body.username
        + "'," +
        req.body.zipcode;
    
      query(qury, function (record) {
        res.json(record);
        });

});

function query(sql, callback) {
    mssql.connect(config, function (err_connect) {
        var cmd = new mssql.Request();
        // var cmd = new mssql.Request()
        cmd.query(sql, function (err_query, data) {
            callback(data);
            mssql.close();
        })
    })
}

module.exports = app;
app.listen(process.env.PORT || 3001);