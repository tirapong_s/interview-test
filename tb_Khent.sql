USE [StandByDB]
GO

/****** Object:  Table [dbo].[tb_Khet]    Script Date: 31/7/2563 18:02:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_Khet](
	[ProvinceID] [int] NULL,
	[KhetID] [int] IDENTITY(1,1) NOT NULL,
	[KhetName] [nvarchar](50) NULL
) ON [PRIMARY]
GO

