USE [StandByDB]
GO

/****** Object:  Table [dbo].[tb_Khwang]    Script Date: 31/7/2563 18:03:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_Khwang](
	[ProvinceID] [int] NULL,
	[KhetID] [int] NULL,
	[KhwangID] [int] IDENTITY(1,1) NOT NULL,
	[KhwangName] [nvarchar](50) NULL,
	[Zipcode] [int] NULL
) ON [PRIMARY]
GO

