USE [StandByDB]
GO

/****** Object:  Table [dbo].[tb_address]    Script Date: 31/7/2563 18:03:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_address](
	[UserID] [int] NULL,
	[ProvinceID] [int] NULL,
	[KhetID] [int] NULL,
	[KhwangID] [int] NULL,
	[Zipcode] [int] NULL
) ON [PRIMARY]
GO

